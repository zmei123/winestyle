<?php

namespace app\models;

use yii\base\Exception;
use yii\base\Model;
use Yii;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * This is the model class for table "sizes".
 *
 * @property int $id
 * @property string $name
 * @property int $height
 * @property int $width
 */
class ImageManager extends Model
{

    CONST EXT = 'jpg';

    public $name;
    public $size;

    /**
     * ImageManager constructor.
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        if (!empty($config['name'])) {
            $this->name = $config['name'];
            if (!empty($config['size'])) {
                $this->size = $config['size'];
            } else {
                if ($this->isMobileDevice()) {
                    $this->size = 'mic';
                } else {
                    $this->size = 'min';
                }
            }
        }
        parent::__construct([]);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'size'], 'required'],
            [['name'], 'string', 'max' => 255],
            ['name', 'match', 'pattern' => '/^[a-zа-я0-9\.\_]+$/i'],
            ['size', 'match', 'pattern' => '/^[a-z]+$/i'],
        ];
    }

    /**
     * Получение изображение по входным параметрам
     * @return array
     */
    public function getImage()
    {
        if ($this->validate()) {
            $image = Image::findOne(['name' => $this->name]);
            $size = Size::findOne(['name' => $this->size]);
            if ($image && $size) {
                $baseFile = Yii::getAlias('@app') . '/data/' . $image->name . '.' . self::EXT;
                $resultFile = Yii::getAlias('@app') . '/web/image/cache/cache_' . $image->name . '_' . $size->name . '.' . self::EXT;
                if (file_exists($resultFile)) {
                    return [
                        'image' => file_get_contents($resultFile),
                        'success' => true
                    ];
                } else {
                    if (file_exists($baseFile)) {
                        $isResize = $this->resize($baseFile, $resultFile, $size->height, $size->width);

                        if ($isResize) {
                            return [
                                'image' => file_get_contents($resultFile),
                                'success' => true
                            ];
                        }
                    }
                }
            }
        }

        return ['success' => false];
    }

    /**
     * Ресайзинг изображения
     * @param $fn - путь к изображению
     * @param $origin - путь к сохраняемому файлу
     * @param $height - высота
     * @param $width - ширина
     */
    public function resize($fn, $origin, $height, $width)
    {
        try {
            $size = getimagesize($fn);
            $ratio = $size[0] / $size[1];
            if ($ratio > 1) {
                $height = $height / $ratio;
            } else {
                $width = $height * $ratio;
            }
            $src = imagecreatefromstring(file_get_contents($fn));
            $dst = imagecreatetruecolor($width, $height);
            imagecopyresampled($dst, $src, 0, 0, 0, 0, $width, $height, $size[0], $size[1]);
            imagedestroy($src);
            imagepng($dst, $origin);
            imagedestroy($dst);

            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Проверка на мобильную версию браузера
     * @return bool
     */
    public static function isMobileDevice()
    {
        $aMobileUA = array(
            '/iphone/i' => 'iPhone',
            '/ipod/i' => 'iPod',
            '/ipad/i' => 'iPad',
            '/android/i' => 'Android',
            '/blackberry/i' => 'BlackBerry',
            '/webos/i' => 'Mobile'
        );

        foreach ($aMobileUA as $sMobileKey => $sMobileOS) {
            if (preg_match($sMobileKey, $_SERVER['HTTP_USER_AGENT'])) {
                return true;
            }
        }

        return false;
    }

    /**
     * Получение списка картинок для карусели
     * @param $image - объект класса Image
     * @return array
     */
    public static function getImagePreviews($image)
    {
        $out = [];
        $sizes = Size::find()->all();
        foreach ($sizes as $size) {
            if (self::isMobileDevice()) {
                if ($size->name == 'big') {
                    continue;
                }
            } else {
                if ($size->name == 'mic') {
                    continue;
                }
            }
            $out[] = Html::img(Url::to(['image/generator', 'name' => $image->name, 'size' => $size->name]));
        }

        return $out;
    }

}
