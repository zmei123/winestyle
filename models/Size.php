<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sizes".
 *
 * @property int $id
 * @property string $name
 * @property int $height
 * @property int $width
 */
class Size extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sizes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'height', 'width'], 'required'],
            [['height', 'width'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'height' => 'Height',
            'width' => 'Width',
        ];
    }
}
