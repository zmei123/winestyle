<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Image */

$this->params['breadcrumbs'][] = ['label' => 'Images', 'url' => ['index']];
\yii\web\YiiAsset::register($this);
?>
<div class="image-view">

    <?= yii\bootstrap\Carousel::widget(['items' => $items]); ?>

</div>
